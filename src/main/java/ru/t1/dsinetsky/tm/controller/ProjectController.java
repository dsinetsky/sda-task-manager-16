package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.controller.IProjectController;
import ru.t1.dsinetsky.tm.api.service.ILoggerService;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.system.InvalidSortTypeException;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(final IProjectService projectService, final IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    private void showProject(Project project) {
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Desc: " + project.getDesc());
        System.out.println("Status: " + Status.toName(project.getStatus()));
    }

    @Override
    public void create() throws GeneralException {
        System.out.println("Enter name of new project:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        projectService.create(name, description);
        System.out.println("Project successfully created!");
    }

    @Override
    public void show() throws InvalidSortTypeException {
        System.out.println("Enter sort type or leave empty for standard:");
        System.out.println(Sort.getSortList());
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> listProjects = projectService.returnAll(sort);
        System.out.println("List of projects:");
        int index = 1;
        for (final Project project : listProjects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
    }

    @Override
    public void clear() {
        projectService.clearAll();
        System.out.println("All projects successfully cleared!");
    }

    @Override
    public void showProjectById() throws GeneralException {
        System.out.println("Enter id of project:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findById(id);
        showProject(project);
    }

    @Override
    public void showProjectByIndex() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        final Project project = projectService.findByIndex(index);
        showProject(project);
    }

    @Override
    public void updateProjectById() throws GeneralException {
        System.out.println("Enter id of project:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter new name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateById(id, name, description);
        showProject(project);
        System.out.println("Project successfully updated!");
    }

    @Override
    public void updateProjectByIndex() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(index, name, description);
        showProject(project);
        System.out.println("Project successfully updated!");
    }

    @Override
    public void removeProjectById() throws GeneralException {
        System.out.println("Enter id of project:");
        final String id = TerminalUtil.nextLine();
        projectTaskService.removeProjectById(id);
        System.out.println("Project successfully removed!");
    }

    @Override
    public void removeProjectByIndex() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        projectTaskService.removeProjectByIndex(index);
        System.out.println("Project successfully removed!");
    }

    @Override
    public void createTestProjects() throws GeneralException {
        projectService.createTestProjects();
        System.out.println("10 test projects created!");
    }

    @Override
    public void startProjectById() throws GeneralException {
        System.out.println("Enter id of project:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, Status.IN_PROGRESS);
        showProject(project);
        System.out.println("Project successfully started!");
    }

    @Override
    public void startProjectByIndex() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        final Project project = projectService.changeStatusByIndex(index, Status.IN_PROGRESS);
        showProject(project);
        System.out.println("Project successfully started!");
    }

    @Override
    public void completeProjectById() throws GeneralException {
        System.out.println("Enter id of project:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, Status.COMPLETED);
        showProject(project);
        System.out.println("Project successfully completed!");
    }

    @Override
    public void completeProjectByIndex() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        final Project project = projectService.changeStatusByIndex(index, Status.COMPLETED);
        showProject(project);
        System.out.println("Project successfully completed!");
    }

    @Override
    public void changeStatusById() throws GeneralException {
        System.out.println("Enter id of project:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        final String newStatus = TerminalUtil.nextLine();
        final Status status = Status.toStatus(newStatus);
        final Project project = projectService.changeStatusById(id, status);
        showProject(project);
        System.out.println("Status successfully changed");
    }

    @Override
    public void changeStatusByIndex() throws GeneralException {
        System.out.println("Enter index of project:");
        final int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        final String newStatus = TerminalUtil.nextLine();
        final Status status = Status.toStatus(newStatus);
        final Project project = projectService.changeStatusByIndex(index, status);
        showProject(project);
        System.out.println("Status successfully changed");
    }

}

package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.controller.ICommandController;
import ru.t1.dsinetsky.tm.api.service.ICommandService;
import ru.t1.dsinetsky.tm.model.Command;
import ru.t1.dsinetsky.tm.util.NumberUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayHelp() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            System.out.println(command);
        }
    }

    @Override
    public void displayVersion() {
        System.out.println("Version: 1.16.0");
    }

    @Override
    public void displayAbout() {
        System.out.println("Developer: Sinetsky Dmitry");
        System.out.println("Dev Email: dsinetsky@t1-consulting.ru");
    }

    @Override
    public String getWelcomeMessage(){
        return "Welcome to the task-manager_16!\nType \"help\" for list of commands";
    }

    @Override
    public void displayWelcome() {
        System.out.println(getWelcomeMessage());
    }

    @Override
    public void displayError() {
        System.out.println("Invalid input! Type \"help\" or use argument \"-h\" for list of inputs");
    }

    @Override
    public void displayInfo() {
        /* Processors, count and print */
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        /* Memory as bytes */
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;
        /* Memory as String */
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryFormat);
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        /* Print memory */
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory: " + totalMemoryFormat);
        System.out.println("Used Memory: " + usedMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);
    }

    @Override
    public void displayArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command.getArgument() != null && !command.getArgument().isEmpty())
                System.out.println(command.getArgument());
        }
    }

    @Override
    public void displayCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            if (command.getName() != null && !command.getName().isEmpty())
                System.out.println(command.getName());
        }
    }

}

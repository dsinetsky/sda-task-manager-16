package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.controller.ITaskController;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.system.InvalidSortTypeException;
import ru.t1.dsinetsky.tm.model.Task;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    private void showTask(Task task) {
        System.out.println("Id: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDesc());
        System.out.println("Status: " + Status.toName(task.getStatus()));
    }

    private void showListTask(List<Task> listTasks) {
        System.out.println("List of tasks:");
        int i = 1;
        for (final Task task : listTasks) {
            System.out.println(i + ". " + task.toString());
            i++;
        }
    }

    @Override
    public void showTasks() throws InvalidSortTypeException {
        System.out.println("Enter sort type or leave empty for standard:");
        System.out.println(Sort.getSortList());
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        showListTask(taskService.returnAll(sort));
    }

    @Override
    public void showTasksByProjectId() throws GeneralException {
        System.out.println("Enter Id of project:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> listTasks = taskService.returnTasksOfProject(projectId);
        showListTask(listTasks);
    }

    @Override
    public void createTask() throws GeneralException {
        System.out.println("Enter name of new task:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("Task successfully created!");
    }

    @Override
    public void clearTasks() {
        taskService.clearAll();
        System.out.println("Tasks successfully cleared!");
    }

    @Override
    public void showTaskById() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        showTask(task);
    }

    @Override
    public void showTaskByIndex() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.findByIndex(index);
        showTask(task);
    }

    @Override
    public void updateTaskById() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter new name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        showTask(task);
        System.out.println("Task successfully updated!");
    }

    @Override
    public void updateTaskByIndex() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter new description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        showTask(task);
        System.out.println("Task successfully updated!");
    }

    @Override
    public void removeTaskById() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
        System.out.println("Task successfully removed!");
    }

    @Override
    public void removeTaskByIndex() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        taskService.removeByIndex(index);
        System.out.println("Task successfully removed!");
    }

    @Override
    public void createTestTasks() throws GeneralException {
        taskService.createTestTasks();
        System.out.println("10 test tasks created!");
    }

    @Override
    public void startTaskById() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, Status.IN_PROGRESS);
        showTask(task);
        System.out.println("Task successfully started!");
    }

    @Override
    public void startTaskByIndex() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.changeStatusByIndex(index, Status.IN_PROGRESS);
        showTask(task);
        System.out.println("Task successfully started!");
    }

    @Override
    public void completeTaskById() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, Status.COMPLETED);
        showTask(task);
        System.out.println("Task successfully completed!");
    }

    @Override
    public void completeTaskByIndex() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.changeStatusByIndex(index, Status.COMPLETED);
        showTask(task);
        System.out.println("Task successfully completed!");
    }

    @Override
    public void changeStatusById() throws GeneralException {
        System.out.println("Enter id of task:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        final String newStatus = TerminalUtil.nextLine();
        final Status status = Status.toStatus(newStatus);
        final Task task = taskService.changeStatusById(id, status);
        showTask(task);
        System.out.println("Status successfully changed");
    }

    @Override
    public void changeStatusByIndex() throws GeneralException {
        System.out.println("Enter index of task:");
        final int index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter new status. Available statuses:");
        System.out.println(Status.getStatusList());
        final String newStatus = TerminalUtil.nextLine();
        final Status status = Status.toStatus(newStatus);
        final Task task = taskService.changeStatusByIndex(index, status);
        showTask(task);
        System.out.println("Status successfully changed");
    }

}

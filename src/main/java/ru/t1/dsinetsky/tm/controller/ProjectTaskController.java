package ru.t1.dsinetsky.tm.controller;

import ru.t1.dsinetsky.tm.api.controller.IProjectTaskController;
import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class ProjectTaskController implements IProjectTaskController {

    private IProjectTaskService projectTaskService;


    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProjectById() throws GeneralException {
        System.out.println("Enter Id of project:");
        final String projectEnter = TerminalUtil.nextLine();
        System.out.println("Enter Id of task:");
        final String taskEnter = TerminalUtil.nextLine();
        projectTaskService.bindProjectById(projectEnter, taskEnter);
        System.out.println("Task successfully bound to project!");
    }

    @Override
    public void unbindTaskToProjectById() throws GeneralException {
        System.out.println("Enter Id of project:");
        final String projectEnter = TerminalUtil.nextLine();
        System.out.println("Enter Id of task:");
        final String taskEnter = TerminalUtil.nextLine();
        projectTaskService.unbindProjectById(projectEnter, taskEnter);
        System.out.println("Task successfully unbound to project!");
    }

}

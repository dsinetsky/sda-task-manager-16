package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task createTask(Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clearTasks() {
        tasks.clear();
    }

    @Override
    public List<Task> returnAll() {
        return tasks;
    }

    @Override
    public List<Task> returnAll(final Comparator comparator) {
        final List<Task> sortedList = new ArrayList<>(tasks);
        sortedList.sort(comparator);
        return sortedList;
    }

    @Override
    public Task findById(final String id) {
        for (final Task task : tasks) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final int index) {
        if (index >= tasks.size()) return null;
        return tasks.get(index);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        else tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        else tasks.remove(task);
        return task;
    }

    @Override
    public List<Task> findTasksByProjectId(final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (projectId.equals(task.getProjectId())) result.add(task);
        }
        return result;
    }

    @Override
    public int getSize() {
        return tasks.size();
    }
}

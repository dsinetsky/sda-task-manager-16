package ru.t1.dsinetsky.tm.exception;

public class GeneralException extends Exception{

    public GeneralException() {
    }

    public GeneralException(final String message) {
        super(message);
    }

    public GeneralException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public GeneralException(final Throwable cause) {
        super(cause);
    }

    public GeneralException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}

package ru.t1.dsinetsky.tm.exception.system;

public final class InvalidCommandException extends GeneralSystemException {

    public InvalidCommandException() {
        super("Invalid command! Type \"help\" for list of commands!");
    }

    public InvalidCommandException(final String message) {
        super("Command " + message + " is not supported! Type \"help\" for list of commands!");
    }

}

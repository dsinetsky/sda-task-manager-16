package ru.t1.dsinetsky.tm.component;

import ru.t1.dsinetsky.tm.api.controller.ICommandController;
import ru.t1.dsinetsky.tm.api.controller.IProjectController;
import ru.t1.dsinetsky.tm.api.controller.IProjectTaskController;
import ru.t1.dsinetsky.tm.api.controller.ITaskController;
import ru.t1.dsinetsky.tm.api.repository.ICommandRepository;
import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.*;
import ru.t1.dsinetsky.tm.constant.ArgumentConst;
import ru.t1.dsinetsky.tm.constant.TerminalConst;
import ru.t1.dsinetsky.tm.controller.CommandController;
import ru.t1.dsinetsky.tm.controller.ProjectController;
import ru.t1.dsinetsky.tm.controller.ProjectTaskController;
import ru.t1.dsinetsky.tm.controller.TaskController;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.system.InvalidArgumentException;
import ru.t1.dsinetsky.tm.exception.system.InvalidCommandException;
import ru.t1.dsinetsky.tm.repository.CommandRepository;
import ru.t1.dsinetsky.tm.repository.ProjectRepository;
import ru.t1.dsinetsky.tm.repository.TaskRepository;
import ru.t1.dsinetsky.tm.service.*;
import ru.t1.dsinetsky.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ILoggerService loggerService = new LoggerService();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);



    private void terminalRun(final String command) throws GeneralException {
        if (command == null) throw new InvalidCommandException();
        switch (command) {
            case (TerminalConst.CMD_HELP):
                commandController.displayHelp();
                break;
            case (TerminalConst.CMD_VERSION):
                commandController.displayVersion();
                break;
            case (TerminalConst.CMD_ABOUT):
                commandController.displayAbout();
                break;
            case (TerminalConst.CMD_INFO):
                commandController.displayInfo();
                break;
            case (TerminalConst.CMD_ARG):
                commandController.displayArguments();
                break;
            case (TerminalConst.CMD_CMD):
                commandController.displayCommands();
                break;
            case (TerminalConst.CMD_PROJECT_CLEAR):
                projectController.clear();
                break;
            case (TerminalConst.CMD_PROJECT_CREATE):
                projectController.create();
                break;
            case (TerminalConst.CMD_PROJECT_LIST):
                projectController.show();
                break;
            case (TerminalConst.CMD_FIND_PROJECT_BY_ID):
                projectController.showProjectById();
                break;
            case (TerminalConst.CMD_FIND_PROJECT_BY_INDEX):
                projectController.showProjectByIndex();
                break;
            case (TerminalConst.CMD_UPD_PROJECT_BY_ID):
                projectController.updateProjectById();
                break;
            case (TerminalConst.CMD_UPD_PROJECT_BY_INDEX):
                projectController.updateProjectByIndex();
                break;
            case (TerminalConst.CMD_REMOVE_PROJECT_BY_ID):
                projectController.removeProjectById();
                break;
            case (TerminalConst.CMD_REMOVE_PROJECT_BY_INDEX):
                projectController.removeProjectByIndex();
                break;
            case (TerminalConst.CMD_PROJECT_START_BY_ID):
                projectController.startProjectById();
                break;
            case (TerminalConst.CMD_PROJECT_START_BY_INDEX):
                projectController.startProjectByIndex();
                break;
            case (TerminalConst.CMD_PROJECT_COMPLETE_ID):
                projectController.completeProjectById();
                break;
            case (TerminalConst.CMD_PROJECT_COMPLETE_INDEX):
                projectController.completeProjectByIndex();
                break;
            case (TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_ID):
                projectController.changeStatusById();
                break;
            case (TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX):
                projectController.changeStatusByIndex();
                break;
            case (TerminalConst.CMD_TASK_CLEAR):
                taskController.clearTasks();
                break;
            case (TerminalConst.CMD_TASK_CREATE):
                taskController.createTask();
                break;
            case (TerminalConst.CMD_TASK_LIST):
                taskController.showTasks();
                break;
            case (TerminalConst.CMD_FIND_TASK_BY_ID):
                taskController.showTaskById();
                break;
            case (TerminalConst.CMD_FIND_TASK_BY_INDEX):
                taskController.showTaskByIndex();
                break;
            case (TerminalConst.CMD_UPD_TASK_BY_ID):
                taskController.updateTaskById();
                break;
            case (TerminalConst.CMD_UPD_TASK_BY_INDEX):
                taskController.updateTaskByIndex();
                break;
            case (TerminalConst.CMD_REMOVE_TASK_BY_ID):
                taskController.removeTaskById();
                break;
            case (TerminalConst.CMD_REMOVE_TASK_BY_INDEX):
                taskController.removeTaskByIndex();
                break;
            case (TerminalConst.CMD_TASK_START_BY_ID):
                taskController.startTaskById();
                break;
            case (TerminalConst.CMD_TASK_START_BY_INDEX):
                taskController.startTaskByIndex();
                break;
            case (TerminalConst.CMD_TASK_COMPLETE_ID):
                taskController.completeTaskById();
                break;
            case (TerminalConst.CMD_TASK_COMPLETE_INDEX):
                taskController.completeTaskByIndex();
                break;
            case (TerminalConst.CMD_TASK_CHANGE_STATUS_BY_ID):
                taskController.changeStatusById();
                break;
            case (TerminalConst.CMD_TASK_CHANGE_STATUS_BY_INDEX):
                taskController.changeStatusByIndex();
                break;
            case (TerminalConst.CMD_CREATE_TEST_PROJECTS):
                projectController.createTestProjects();
                break;
            case (TerminalConst.CMD_CREATE_TEST_TASKS):
                taskController.createTestTasks();
                break;
            case (TerminalConst.CMD_BIND_TASK_TO_PROJECT):
                projectTaskController.bindTaskToProjectById();
                break;
            case (TerminalConst.CMD_UNBIND_TASK_TO_PROJECT):
                projectTaskController.unbindTaskToProjectById();
                break;
            case (TerminalConst.CMD_LIST_TASKS_OF_PROJECT):
                taskController.showTasksByProjectId();
                break;
            case (TerminalConst.CMD_EXIT):
                exitApp();
                break;
            default:
                throw new InvalidCommandException(command);
        }
    }

    private void argumentRun(final String arg) throws InvalidArgumentException {
        if (arg == null) throw new InvalidArgumentException();
        switch (arg) {
            case (ArgumentConst.CMD_HELP):
                commandController.displayHelp();
                break;
            case (ArgumentConst.CMD_VERSION):
                commandController.displayVersion();
                break;
            case (ArgumentConst.CMD_ABOUT):
                commandController.displayAbout();
                break;
            case (ArgumentConst.CMD_INFO):
                commandController.displayInfo();
                break;
            case (ArgumentConst.CMD_CMD):
                commandController.displayCommands();
                break;
            case (ArgumentConst.CMD_ARG):
                commandController.displayArguments();
                break;
            default:
                throw new InvalidArgumentException(arg);
        }
    }

    private boolean argRun(final String[] args) throws InvalidArgumentException {
        if (args.length < 1) {
            return false;
        }
        final String param = args[0];
        argumentRun(param);
        return true;
    }


    private void exitApp() {
        System.exit(0);
    }

    private void initLogger(){
        loggerService.info(commandController.getWelcomeMessage());
        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                loggerService.info("Thank you for using task-manager!");
            }
        });
    }

    public void run(final String[] args) {
        try {
            if (argRun(args)) {
                exitApp();
                return;
            }
        } catch (final InvalidArgumentException e) {
            System.err.println(e.getMessage());
            exitApp();
            return;
        }
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nEnter command:");
                final String command = TerminalUtil.nextLine();
                loggerService.command(command);
                terminalRun(command);
            } catch (final GeneralException e) {
                loggerService.error(e);
            }
        }
    }

}

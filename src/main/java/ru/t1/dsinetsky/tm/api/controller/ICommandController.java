package ru.t1.dsinetsky.tm.api.controller;

public interface ICommandController {

    void displayHelp();

    void displayVersion();

    void displayAbout();

    void displayWelcome();

    void displayError();

    void displayInfo();

    void displayArguments();

    void displayCommands();

    String getWelcomeMessage();

}

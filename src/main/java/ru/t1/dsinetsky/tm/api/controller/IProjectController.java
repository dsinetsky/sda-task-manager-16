package ru.t1.dsinetsky.tm.api.controller;

import ru.t1.dsinetsky.tm.exception.GeneralException;

public interface IProjectController {

    void create() throws GeneralException;

    void show() throws GeneralException;

    void clear();

    void showProjectById() throws GeneralException;

    void showProjectByIndex() throws GeneralException;

    void updateProjectById() throws GeneralException;

    void updateProjectByIndex() throws GeneralException;

    void removeProjectById() throws GeneralException;

    void removeProjectByIndex() throws GeneralException;

    void createTestProjects() throws GeneralException;

    void startProjectById() throws GeneralException;

    void startProjectByIndex() throws GeneralException;

    void completeProjectById() throws GeneralException;

    void completeProjectByIndex() throws GeneralException;

    void changeStatusById() throws GeneralException;

    void changeStatusByIndex() throws GeneralException;

}

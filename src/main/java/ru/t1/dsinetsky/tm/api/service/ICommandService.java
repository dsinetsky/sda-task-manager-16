package ru.t1.dsinetsky.tm.api.service;

import ru.t1.dsinetsky.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}

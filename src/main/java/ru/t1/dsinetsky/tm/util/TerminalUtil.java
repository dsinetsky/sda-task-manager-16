package ru.t1.dsinetsky.tm.util;

import ru.t1.dsinetsky.tm.service.LoggerService;

import java.util.Scanner;

public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        final String line = SCANNER.nextLine();
        LoggerService.getMessageLogger().fine(line);
        return line;
    }

    static int nextInt() {
        final String value = nextLine();
        LoggerService.getMessageLogger().fine(value);
        return Integer.parseInt(value);
    }

}

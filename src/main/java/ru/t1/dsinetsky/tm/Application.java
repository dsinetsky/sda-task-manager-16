package ru.t1.dsinetsky.tm;

import ru.t1.dsinetsky.tm.component.Bootstrap;

public class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}


package ru.t1.dsinetsky.tm.model;

public final class Command {

    private String name = "";

    private String argument;

    private String desc = "";

    public Command() {
    }

    public Command(final String name, final String argument, final String desc) {
        this.name = name;
        this.argument = argument;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(final String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(final String argument) {
        this.argument = argument;
    }

    @Override
    public String toString() {
        String result = "";
        if (argument != null && !argument.isEmpty()) result += argument + ",";
        if (name != null && !name.isEmpty()) result += name + " - ";
        if (desc != null && !desc.isEmpty()) result += desc;
        return result;
    }
}

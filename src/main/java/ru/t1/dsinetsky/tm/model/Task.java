package ru.t1.dsinetsky.tm.model;

import ru.t1.dsinetsky.tm.api.model.IWBS;
import ru.t1.dsinetsky.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public final class Task implements IWBS {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String desc = "";

    private Status status = Status.NOT_STARTED;

    private String projectId;

    private Date created = new Date();

    public Task() {
    }

    public Task(final String name) {
        this.name = name;
    }

    public Task(final String name, final String desc) {
        this.name = name;
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(final String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(final String projectId) {
        this.projectId = projectId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return new StringBuilder().append(name).append(" : ").append(desc).toString();
    }
}

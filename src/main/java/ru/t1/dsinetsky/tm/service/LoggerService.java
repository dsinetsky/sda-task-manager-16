package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    private static final String CONFIG_FILE = "/logger.properties";

    private static final String MESSAGE = "message";

    private static final String MESSAGE_FILE = "./message.log";

    private static final String COMMAND = "command";

    private static final String COMMAND_FILE = "./command.log";

    private static final String ERROR = "error";

    private static final String ERROR_FILE = "./error.log";

    private final LogManager manager = LogManager.getLogManager();

    private final Logger root = Logger.getLogger("");

    private static final Logger MESSAGE_LOGGER = getMessageLogger();

    private static final Logger ERROR_LOGGER = getErrorLogger();

    private static final Logger COMMAND_LOGGER = getCommandLogger();

    private final ConsoleHandler consoleHandler = getConsoleHandler();

    public static Logger getMessageLogger() {
        return Logger.getLogger(MESSAGE);
    }

    public static Logger getErrorLogger() {
        return Logger.getLogger(ERROR);
    }

    public static Logger getCommandLogger() {
        return Logger.getLogger(COMMAND);
    }

    {
        init();
        initLogger(MESSAGE_LOGGER,MESSAGE_FILE,true);
        initLogger(ERROR_LOGGER,ERROR_FILE,true);
        initLogger(COMMAND_LOGGER,COMMAND_FILE,false);
    }

    private ConsoleHandler getConsoleHandler() {
        final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter(){

            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private void init(){
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(CONFIG_FILE));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }

    private void initLogger(final Logger logger, final String fileName, final boolean isConsole){
        try {
            if(isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            logger.addHandler(new FileHandler(fileName));
        } catch (final IOException e) {
            root.severe(e.getMessage());
        }
    }


    @Override
    public void info(final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGE_LOGGER.info(message);
    }

    @Override
    public void debug(final String message) {
        if (message == null || message.isEmpty()) return;
        MESSAGE_LOGGER.fine(message);
    }

    @Override
    public void error(final Exception e) {
        if (e == null) return;
        ERROR_LOGGER.log(Level.SEVERE,e.getMessage(),e);
    }

    @Override
    public void command(final String message) {
        if (message == null || message.isEmpty()) return;
        COMMAND_LOGGER.info(message);
    }
}

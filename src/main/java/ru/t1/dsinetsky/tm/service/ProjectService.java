package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.GeneralEntityException;
import ru.t1.dsinetsky.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.*;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.Comparator;
import java.util.List;
import java.util.Random;

public final class ProjectService implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> returnAll() {
        return projectRepository.returnAll();
    }

    @Override
    public List<Project> returnAll(final Comparator comparator) {
        if (comparator == null) return returnAll();
        return projectRepository.returnAll(comparator);
    }

    @Override
    public List<Project> returnAll(final Sort sort) {
        if (sort == null) return returnAll();
        return projectRepository.returnAll(sort.getComparator());
    }

    @Override
    public Project create(final String name) throws GeneralException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        return add(new Project(name));
    }

    @Override
    public Project create(final String name, final String description) throws GeneralException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescIsEmptyException();
        return add(new Project(name, description));
    }

    @Override
    public Project add(final Project project) throws GeneralEntityException {
        if (project == null) throw new ProjectNotFoundException();
        return projectRepository.add(project);
    }

    @Override
    public void clearAll() {
        projectRepository.clearAll();
    }

    @Override
    public Project findById(final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new ProjectIdIsEmptyException();
        final Project project = projectRepository.findById(id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project findByIndex(final int index) throws GeneralFieldException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= projectRepository.getSize()) throw new IndexOutOfSizeException(projectRepository.getSize());
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project removeById(final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new ProjectIdIsEmptyException();
        final Project project = projectRepository.removeById(id);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Override
    public Project removeByIndex(final int index) throws GeneralFieldException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= projectRepository.getSize()) throw new IndexOutOfSizeException(projectRepository.getSize());
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project updateById(final String id, final String name, final String description) throws GeneralException {
        if (id == null || id.isEmpty()) throw new ProjectIdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Project project = findById(id);
        project.setName(name);
        project.setDesc(description);
        return project;
    }

    @Override
    public Project updateByIndex(final int index, final String name, final String description) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= projectRepository.getSize()) throw new IndexOutOfSizeException(projectRepository.getSize());
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Project project = findByIndex(index);
        project.setName(name);
        project.setDesc(description);
        return project;
    }

    @Override
    public Project changeStatusById(final String id, final Status status) throws GeneralException {
        if (id == null || id.isEmpty()) throw new ProjectIdIsEmptyException();
        final Project project = findById(id);
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final int index, final Status status) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= projectRepository.getSize()) throw new IndexOutOfSizeException(projectRepository.getSize());
        final Project project = findByIndex(index);
        project.setStatus(status);
        return project;
    }

    @Override
    public void createTestProjects() throws GeneralException {
        final Random randomizer = new Random();
        final String name = "project";
        final String description = "desc";
        for (int i = 1; i < 11; i++) {
            final Project project = create(name + randomizer.nextInt(11), description + randomizer.nextInt(11));
            final Status status;
            switch (randomizer.nextInt(3)) {
                case 1:
                    status = Status.IN_PROGRESS;
                    break;
                case 2:
                    status = Status.COMPLETED;
                    break;
                default:
                    status = Status.NOT_STARTED;
                    break;
            }
            project.setStatus(status);
        }
    }

}

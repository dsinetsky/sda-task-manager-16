package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.ITaskService;
import ru.t1.dsinetsky.tm.enumerated.Sort;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.exception.GeneralException;
import ru.t1.dsinetsky.tm.exception.entity.TaskNotFoundException;
import ru.t1.dsinetsky.tm.exception.field.*;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.Comparator;
import java.util.List;
import java.util.Random;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void clearAll() {
        taskRepository.clearTasks();
    }

    @Override
    public List<Task> returnAll() {
        return taskRepository.returnAll();
    }

    @Override
    public List<Task> returnAll(final Sort sort) {
        if (sort == null) return returnAll();
        return returnAll(sort.getComparator());
    }

    @Override
    public List<Task> returnAll(final Comparator comparator) {
        if (comparator == null) return returnAll();
        return taskRepository.returnAll(comparator);
    }

    @Override
    public Task create(final String name) throws GeneralFieldException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        return taskRepository.createTask(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) throws GeneralFieldException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescIsEmptyException();
        return taskRepository.createTask(new Task(name, description));
    }

    @Override
    public Task findById(final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        final Task task = taskRepository.findById(id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task findByIndex(final int index) throws GeneralFieldException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= taskRepository.getSize()) throw new IndexOutOfSizeException(taskRepository.getSize());
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task removeById(final String id) throws GeneralException {
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        final Task task = taskRepository.removeById(id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task removeByIndex(final int index) throws GeneralFieldException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= taskRepository.getSize()) throw new IndexOutOfSizeException(taskRepository.getSize());
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws GeneralException {
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Task task = findById(id);
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    public Task updateByIndex(final int index, final String name, final String description) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Task task = findByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDesc(description);
        return task;
    }

    @Override
    public Task changeStatusById(final String id, final Status status) throws GeneralException {
        if (id == null || id.isEmpty()) throw new TaskIdIsEmptyException();
        final Task task = findById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final int index, final Status status) throws GeneralException {
        if (index < 0) throw new NegativeIndexException();
        if (index >= taskRepository.getSize()) throw new IndexOutOfSizeException(taskRepository.getSize());
        final Task task = findByIndex(index);
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> returnTasksOfProject(final String projectId) throws GeneralFieldException {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdIsEmptyException();
        return taskRepository.findTasksByProjectId(projectId);
    }

    @Override
    public void createTestTasks() throws GeneralException {
        final Random randomizer = new Random();
        final String name = "task";
        final String description = "desc";
        for (int i = 1; i < 11; i++) {
            final Task task = create(name + randomizer.nextInt(11), description + randomizer.nextInt(11));
            final Status status;
            switch (randomizer.nextInt(3)) {
                case 1:
                    status = Status.IN_PROGRESS;
                    break;
                case 2:
                    status = Status.COMPLETED;
                    break;
                default:
                    status = Status.NOT_STARTED;
                    break;
            }
            task.setStatus(status);
        }
    }

}
